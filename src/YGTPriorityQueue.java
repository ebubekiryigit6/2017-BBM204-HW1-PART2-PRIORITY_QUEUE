import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;
import java.lang.NullPointerException;

/**
 * Created by Ebubekir.
 */

@SuppressWarnings("unchecked")   // ignores unsafe operation warnings like object cast etc.
public class YGTPriorityQueue<Vector> {

    private static final int INITIAL_SIZE = 32;   // default size if it is not given by user.

    private Object[] array;
    private int size = 0;   // queue size
    private Comparator<Vector> comparator;

    /**
     * Queue constructor, initialize the object array and Comparator
     * @param initialSize array's initial size (if initial size <= 0, constructor assigns default size)
     * @param comparable class comparator
     */
    public YGTPriorityQueue(int initialSize, Comparator<Vector> comparable){
        if (comparable == null){
            throw new NullPointerException("Comparator must be @NOT NULL");
        }
        if (initialSize <= 0){
            array = new Object[INITIAL_SIZE];
            comparator = comparable;
        }
        else {
            array = new Object[initialSize];
            comparator = comparable;
        }
    }

    /**
     * Queue constructor, initialize the object array with default initializer and Comparator
     * @param comparable class comparator
     */
    public YGTPriorityQueue(Comparator<Vector> comparable){
        if (comparable == null){
            throw new NullPointerException("Comparator must be @NOT NULL");
        }
        size = INITIAL_SIZE;
        array = new Object[INITIAL_SIZE];
        comparator = comparable;
    }

    /**
     * Inserts the specified element into this priority queue.
     * @param vector the specified element
     */
    public void add(Vector vector){
        if (vector == null){
            throw new NullPointerException("Object cannot @NULL");
        }
        controlSize();  // controls array size

        int notIncreasedSize = size;    // old last object
        size = notIncreasedSize + 1;    // new size

        // if array is empty, adds object in the beginning of array
        if (notIncreasedSize == 0){
            array[0] = vector;
        }
        else {
            // sift up object
            siftUp(vector,notIncreasedSize);
        }
    }

    /**
     * Removes a single instance of the specified element from this queue if it is present.
     * @return the specified removed element
     */
    public Vector removeMaxPriority(){
        // no element in array
        if (length() == 0){
            throw new  NoSuchElementException("There is no element in Queue!");
        }

        // get the highest priority element
        Vector result = (Vector) array[0];
        // decrease the size, swap first and last element
        array[0] = array[--size];
        // free object place
        array[size] = null;

        if (length() > 0){
            // sift down object
            siftDown(0);
        }
        // return max priority
        return result;
    }

    /**
     * Gets the max priority element
     * @return max priority element
     */
    public Vector getMaxPriority(){
        if (length() == 0){
            throw new NoSuchElementException("There is no element in Queue!");
        }
        // returns max priority element
        return (Vector) array[0];
    }

    /**
     * It replaces the element with comparator, taking it to where it actually should be.
     * @param index which index sifting down
     * @return boolean completed.
     */
    private boolean siftDown(int index){
        if (index < 0){
            throw new ArrayIndexOutOfBoundsException("Index must be greater than zero!");
        }
        int smallestIndex;
        int leftIndex;
        int rightIndex;

        if (index != 0) {
            leftIndex = 2 * index + 1;
            rightIndex = 2 * index + 2;
        }
        else {
            leftIndex = 1;
            rightIndex = 2;
        }

        // it can be out of bound in array.
        if (leftIndex < size){
            if (comparator.compare((Vector) array[leftIndex],(Vector) array[index]) > 0){
                smallestIndex = leftIndex;
            }
            else {
                // index has highest priority
                smallestIndex = index;
            }
        }
        else {
            // has not left index
            smallestIndex = index;
        }

        if (rightIndex < size){
            // compare smallest index of child
            if (comparator.compare((Vector) array[rightIndex], (Vector) array[smallestIndex]) > 0){
                smallestIndex = rightIndex;
            }
        }

        // do not swap same objects, Not exception, time is wasting.
        if (smallestIndex != index){
            swap(index,smallestIndex);
            // call the recursive for its child
            siftDown(smallestIndex);
        }

        return true;
    }

    /**
     * It replaces the element with comparator, taking it to where it actually should be.
     * @param vector element which array's last
     * @param notIncreasedSize which index sifting up
     */
    private void siftUp(Vector vector, int notIncreasedSize){
        while (notIncreasedSize > 0){
            // find the parent
            int parentIndex = (notIncreasedSize-1) / 2;
            Object parentNode = array[parentIndex];
            if (comparator.compare((Vector) parentNode,vector) >= 0){
                break;
            }
            array[notIncreasedSize] = parentNode;
            notIncreasedSize = parentIndex;
        }
        // swap the nodes
        array[notIncreasedSize] = vector;
    }

    /**
     * Controls size of array. If capacity is low, it increases array size.
     */
    private void controlSize(){
        // almost out of bound exception
        if (size == getArrayCapacity()-1){
            int oldSize = getArrayCapacity();
            int newSize = oldSize * 2;
            array = Arrays.copyOf(array,newSize);
        }
    }

    /**
     * Swaps the given index of array
     * @param i swapped index
     * @param j swapped index
     */
    private void swap(int i, int j){
        if (i >= size || j >= size){
            throw new ArrayIndexOutOfBoundsException();
        }
        Vector temp = (Vector) array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    /**
     * @return length of array
     */
    public int length(){
        return size;
    }

    /**
     * @return true if array is empty.
     */
    public boolean isEmpty(){
        return size == 0;
    }

    /**
     * @return Allocated array capacity.
     */
    private int getArrayCapacity(){
        return array.length;
    }

}
