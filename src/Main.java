import java.awt.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        TouristicPlaces touristic = new TouristicPlaces();  // for place functions
        Point alicesCoordinate = new Point(touristic.getRandomXYCoordinate(),touristic.getRandomXYCoordinate()); // sets the alice's coordinates


        /**
         *  Creates a Priority Queue, The element with the highest priority is the smallest element of the "VISIT FEE".
         */

        YGTPriorityQueue<Coordinate> feeBasedPriorityQueue = new YGTPriorityQueue<Coordinate>(3, new Comparator<Coordinate>() {
            @Override
            public int compare(Coordinate o1, Coordinate o2) {
                if (o1.getFee() < o2.getFee()){
                    // max priority element has the smallest visit fee
                    return 1;
                }
                else if (o1.getFee() > o2.getFee()){
                    return -1;
                }
                else {
                    return 0;
                }
            }
        });

        /**
         * Creates a Priority Queue, The element with the highest priority is the smallest element of the "DISTANCE".
         */
        YGTPriorityQueue<Coordinate> distanceBasedPriorityQueue = new YGTPriorityQueue<Coordinate>(3, new Comparator<Coordinate>() {
            @Override
            public int compare(Coordinate o1, Coordinate o2) {
                double firstDistance = o1.calculateDistanceTo(alicesCoordinate);
                double secondDistance = o2.calculateDistanceTo(alicesCoordinate);
                if (firstDistance < secondDistance){
                    return 1;
                }
                else if (firstDistance > secondDistance){
                    return -1;
                }
                else {
                    return 0;
                }
            }
        });


        int numOfPlaces = 100;   // how many random places
        int MAX_DISTANCE = 200;  // max distance limit


        for (int i = 0; i < numOfPlaces; i++){
            // create the random coordinates
            Coordinate coordinate = touristic.getRandomCoordinate(i+1);
            if (coordinate.calculateDistanceTo(alicesCoordinate) <= MAX_DISTANCE){
                // less than 200
                feeBasedPriorityQueue.add(coordinate);
            }
            else {
                // greater than 200
                distanceBasedPriorityQueue.add(coordinate);
            }
        }

        // to give information about queues
        System.out.printf("Fee based size: %d\nDistance based size: %d\n",feeBasedPriorityQueue.length(),distanceBasedPriorityQueue.length());


        int CRITERIA;   // how many element will print

        while (true) {
            try {
                System.out.print("Enter the N value: ");
                Scanner sc = new Scanner(System.in);
                CRITERIA = sc.nextInt();

                if (CRITERIA <= 100 && CRITERIA >= 0){
                    break;
                }
                else {
                    System.out.printf("The input must be between zero and one hundred.\n");
                }
            } catch (Exception e){
                // if the input not integer
                System.out.println("You entered wrong input type. Please enter an integer!");
            }
        }

        if (CRITERIA != 0) {
            // if input is zero, don't write "distances are found!"
            System.out.printf("The booster distances are found!\n");
        }


        int counter = 1;
        for (int i = 0; i < CRITERIA; i++){
            Coordinate temp;
            try {
                temp = feeBasedPriorityQueue.removeMaxPriority();
            } catch (NoSuchElementException e){
                // there is no element in fee based queue
                temp = distanceBasedPriorityQueue.removeMaxPriority();
            }

            if (counter != 1)
                System.out.println();

            // print the information
            System.out.printf(touristic.ordinal(counter) + " nearest distance calculated with the "
                    + touristic.ordinal(temp.getId())
                    + " generated coordinate is %.0f\n"
                    +"Coordinates of touristic place is (%d,%d), location fee is %d\n",
                    temp.calculateDistanceTo(alicesCoordinate),
                    temp.getxCoordinate(),
                    temp.getyCoordinate(),
                    temp.getFee());

            counter++;
        }

    }
}
