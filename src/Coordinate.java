import java.awt.*;

/**
 * Created by Ebubekir
 *
 */

public class Coordinate extends TouristicPlaces {

    private int id;  // object id
    private int xCoordinate;
    private int yCoordinate;
    private int fee;    // visit fee


    public Coordinate(int id,int xCoordinate, int yCoordinate, int fee){
        this.id = id;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.fee = fee;
    }

    public int getId() {
        return id;
    }

    public int getFee() {
        return fee;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }


    /**
     * Calculates the distance to another location.
     * @param point another point
     * @return returns distance
     */
    public double calculateDistanceTo(Point point){
        if (point == null){
            throw new NullPointerException("A point cannot be @NULL");
        }
        return Math.sqrt(Math.pow(point.getX() - this.xCoordinate,2) + Math.pow(point.getY() - this.yCoordinate,2));
    }
}
