import java.util.Random;

/**
 * Created by Ebubekir
 */
public class TouristicPlaces {

    /**
     * gets random coordinate object
     * @param id sets the coordinate id
     * @return random coordinate
     */
    public Coordinate getRandomCoordinate(int id){
        return new Coordinate(id,getRandomXYCoordinate(),getRandomXYCoordinate(),getRandomVisitFee());
    }

    /**
     * Gives a random value between 0 and 60
     * @return random int value 0 <= x <= 60
     */
    public int getRandomVisitFee(){
        Random random = new Random();
        return random.nextInt(61);   //   0 <=  x  <= 60
    }

    /**
     * Gives a random value between -1000 and 1000
     * @return random int value -1000 <= x <= 1000
     */
    public int getRandomXYCoordinate(){
        Random random = new Random();
        return random.nextInt(2001) - 1000;   //  -1000  <=  x  <= 1000
    }

    /**
     *
     * @param i integer number
     * @return a string that becomes ordinal value of given integer
     */
    public String ordinal(int i) {
        String[] sufixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + sufixes[i % 10];
        }
    }


}
